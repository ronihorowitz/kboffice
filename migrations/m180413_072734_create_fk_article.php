<?php

use yii\db\Migration;

/**
 * Class m180413_072734_create_fk_article
 */
class m180413_072734_create_fk_article extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /*
        $this->addForeignKey(
            'fk-product-category_id',
            'article',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );
        */
        $this->addForeignKey(
            'fk-product-author_id',
            'article',
            'author_id',
            'user',
            'id'
        ); 
        $this->addForeignKey(
            'fk-product-editor_id',
            'article',
            'editor_id',
            'user',
            'id'
        ); 
        $this->addForeignKey(
            'fk-product-created_by',
            'article',
            'created_by',
            'user',
            'id'
        ); 
        $this->addForeignKey(
            'fk-product-updated_by',
            'article',
            'updated_by',
            'user',
            'id'
        );                                 
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180413_072734_create_fk_article cannot be reverted.\n";

        $this->dropForeignKey(
            'fk-product-author_id',
            'user'
        );
        $this->dropForeignKey(
            'fk-product-editor_id',
            'user'
        );  
        $this->dropForeignKey(
            'fk-product-created_by',
            'user'
        );  
        $this->dropForeignKey(
            'fk-product-updated_by',
            'user'
        );                       
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180413_072734_create_fk_article cannot be reverted.\n";

        return false;
    }
    */
}
